﻿using knopka.Models;
using knopka.Repository;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace knopka.Services
{
    public class DataService : IDataService
    {
        private IHttpClientFactory _httpClientFactory;
        private readonly IAlbumRepository _albumRepository;

        public DataService(IHttpClientFactory httpClientFactory,
            IAlbumRepository albumRepository)
        {
            _httpClientFactory = httpClientFactory;
            _albumRepository = albumRepository;
        }

        public async Task<bool> GetData()
        {
            var result = await Get();
            var albums = await Task.Run(() => Parse(result));

            return await Save(albums);
        }

        public async Task<string> Get()
        {
            var apiClient = _httpClientFactory.CreateClient();
            var response = await apiClient.GetAsync("https://jsonplaceholder.typicode.com/albums");

            return await response.Content.ReadAsStringAsync();
        }

        public IEnumerable<Album> Parse(string str)
        {
            var albums = new List<Album>();

            var res = JArray.Parse(str);
            foreach (var i in res)
            {
                albums.Add(new Album { 
                    UserId = (int)i["userId"],
                    ExtId = (int)i["id"],
                    Title = i["title"].ToString(),
                });
            }

            return albums;
        }

        public async Task<bool> Save(IEnumerable<Album> albums)
        {
            foreach (var album in albums)
            {
                await _albumRepository.Add(album);
            }

            return true;
        }
    }
}
